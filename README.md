# Continuous Integration Assignment
Git: https://gitlab.com/bolleyboll/nht-ci
Last commit ID is: 705220e4347f3c8676280513c6d9320f2198ec1e. This will be the second last commit as this Readme was updated after this.
Screenshots can be found in the [screenshot](https://gitlab.com/bolleyboll/nht-ci/-/tree/main/screenshot) directory.


## GitLab CI/CD Configuration (`gitlab-ci.yml`):

This GitLab CI/CD configuration defines a pipeline with three stages: `build`, `login`, and `push`. Each stage consists of specific jobs to execute.
Before entering any stages it changes directory to `tcp-echo-server`, checks Maven version and exports PATH variable as it is running on the local system so I needed to manually point to the homebrew installation location.

- The `build` stage:
  - It uses the `local` runner.
  - The `mvn clean package` command is used to build the Maven project.
  - A Docker image is built using the `docker build` command.

- The `docker_login` stage:
  - It logs in to Docker Hub using the provided username and `$hubpassword` (secure variable).

- The `dockerhub_push` stage:
  - It pushes the Docker image to Docker Hub.

## Jenkins Pipeline (`Jenkinsfile`):

This Jenkins Pipeline is a declarative pipeline that performs the following steps:

1. **Git Pull Stage**:
   - Pulls the code from the specified Git repository on the `main` branch.

2. **NPM Build Stage**:
   - Enters the `node-hello-world` directory.
   - Runs `npm install` to install Node.js dependencies.

3. **Docker Image Build Stage**:
   - Builds a Docker image tagged as `freshlyjuiced/hello-world:latest`.

4. **Push Docker Image Stage**:
   - Pushes the built Docker image to a Docker registry using the provided credentials.
