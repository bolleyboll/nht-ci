FROM node:18
WORKDIR /node-hello-world
COPY ./node-hello-world /node-hello-world
RUN npm install 
ENTRYPOINT npm start
